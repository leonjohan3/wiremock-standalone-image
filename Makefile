THIS_IMAGE_NAME = wiremock-standalone
DOCKER_IMAGE_NAME = ubuntu-java-generic
#DOCKER_IMAGE_NAME = leonjohan3atyahoodotcom/ubuntu-java-generic
include ./.s2i/environment

.PHONY: build
build:
	git diff --quiet
	test -z "$$(git status --porcelain)" || exit 1
	s2i build . $(DOCKER_IMAGE_NAME) $(THIS_IMAGE_NAME) --incremental=true

.PHONY: deploy
deploy: build
	docker run --restart always -d -u default --name wiremock-standalone -p 9999:8080 $(THIS_IMAGE_NAME)
