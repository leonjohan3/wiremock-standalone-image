# Overview
This is the [s2i](<https://docs.openshift.com/enterprise/3.0/creating_images/s2i.html>) source for the [Wiremock Standalone](<http://wiremock.org/docs/running-standalone/>) image and has a dependency on https://bitbucket.org/leonjohan3/ubuntu-java-generic-image. This project is related to https://bitbucket.org/leonjohan3/iot-spring-integration-gcp-pubsub.

# Notes
1.  Set the required Wiremock Standalone version to use in the .s2i/environment file.
2.  To build and deploy the Docker image, run `make deploy`.
